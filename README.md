# Robot Challenge
API to manage a robot moving through a grid surface  

## Dev system requirements
- yarn
- node & nvm
- parcel

## Cold boot
- `yarn install`

## Dev commands
- `yarn test` runs commands tests 
- `yarn test-console` runs commands tests and outputs logs to see what actions are happening
- `yarn dev` runs parcel for development in browser
- `yarn build` runs parcel to build release in **dist** dir