export class Vec2D {
    public x: number
    public y: number
    constructor (x:number, y:number) {
        this.x = x
        this.y = y
    }

    /**
     * Adds vector to current and returns result
     * @param {Vec2D} vec vector to add
     */
    add(vec:Vec2D):Vec2D {
        return new Vec2D(this.x + vec.x, this.y + vec.y)
    }

    /**
     * Performs quick comparison with another {Vec2D}
     * @param {Vec2D} vec the vector to compare with
     */
    equals(vec:Vec2D):boolean {
        return this.x == vec.x && this.y == vec.y
    }
}

export enum Direction { N, E, S, W }

export enum InstructionMap { f = 1, b = -1, r = 1, l = -1 }