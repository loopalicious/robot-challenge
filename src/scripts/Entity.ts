import { Vec2D, Direction } from './Utils'

export default class {
    public positions:Vec2D[] = []
    public direction:Direction

    protected _instructions:string = ''

    constructor (spawnPt:Vec2D, direction:Direction) {
        this.positions.push(spawnPt)
        this.direction = direction
    }

    get position ():Vec2D {
        return this.positions[this.positions.length -1]
    }

    set position (val:Vec2D) {
        this.positions.push(val)
    }

    get instructions ():string {
        return this._instructions
    }
    
    // The robot receives an array of commands.
    pushInstructions (comms:string) {
        this._instructions += comms
    }

    /**
     * Fetches next instructions an shifts form instruction stack
     * @returns {string} the next instruction from stack
     */
    fetchNextInstruction ():string {
        let list:string[] = this._instructions.split('')
        let nextInstruction = list.shift()
        this._instructions = list.join('')
        return nextInstruction
    }

    describe ():string {
        return `Entity at: ${JSON.stringify(this.position)} facing: ${Direction[this.direction]}`
    }


}