import RobotAPI from "./RobotAPI"
import { Direction, Vec2D } from "./Utils"

// silence console logs
if (!process.env.CONSOLE) {
    console.log = jest.fn()
}

let api:RobotAPI

describe('Pre-checks', () => {
    test('Instantiate API', () =>{
        api = new RobotAPI(new Vec2D(100, 100))
        expect(api).toBeInstanceOf(RobotAPI)
    })

    test('Instantiate API with invalid grid size should throw error', () =>{
        expect(() => { new RobotAPI(new Vec2D(-1, -1)) }).toThrowError()
    })

    test('Add robot out of grid bounds should throw error', () =>{
        expect(() => {
             new RobotAPI(new Vec2D(10, 10))
                .createRobot(new Vec2D(20, 20), Direction.N) 
        }).toThrowError()
    })

})

describe('Scenario 1:', () => {
    // The robot is on a 100×100 grid at location (0, 0) and facing SOUTH.
    // The robot is given the commands “fflff” and should end up at (2, 2)
    test('Setup grid 100x100', () =>{
        api = new RobotAPI(new Vec2D(100, 100))
        expect(api.gridSize).toMatchObject(new Vec2D(100, 100))
    })

    test('Create robot at 0,0 facing South', () =>{
        api.createRobot(new Vec2D( 0, 0), Direction.S)
        expect(api.robot.position).toMatchObject(new Vec2D(0, 0))
        expect(api.robot.direction).toBe(Direction.S)
    })

    test('Send instructions (fflff) to robot and execute', () =>{
        let inst:string = 'fflff'
        api.robot.pushInstructions(inst)
        // check instructions are correct
        expect(api.robot.instructions).toMatch(inst)
        // check all instructions could run
        expect(api.run()).toBe(true)
    })

    test('Check robot ends up at 2,2', () =>{  
        expect(api.robot.position).toMatchObject(new Vec2D( 2, 2))
    })
})

describe('Scenario 2:', () => {
    // The robot is on a 50×50 grid at location (1, 1) and facing NORTH. 
    // The robot is given the commands “fflff” and should end up at (1, 0)

    test('Setup grid 50x50', () =>{
        api = new RobotAPI(new Vec2D(50, 50))
        expect(api.gridSize).toMatchObject(new Vec2D(50, 50))
    })

    test('Create robot at 1,1 facing North', () =>{
        api.createRobot(new Vec2D(1, 1), Direction.N)
        expect(api.robot.position).toMatchObject(new Vec2D(1, 1))
        expect(api.robot.direction).toBe(Direction.N)
    })

    test('Send instructions (fflff) to robot and execute', () =>{
        let inst:string = 'fflff'
        api.robot.pushInstructions(inst)
        // check instructions are correct
        expect(api.robot.instructions).toMatch(inst)
        // check that *NOT* all instructions could run
        expect(api.run()).toBe(false)
    })

    test('Check robot ends up at 1,0', () =>{  
        expect(api.robot.position).toMatchObject(new Vec2D( 1, 0))
    })
})


describe('Scenario 3:', () => {
    // The robot is on a 100×100 grid at location (50, 50) and facing NORTH. 
    // The robot is given the commands “fflffrbb” but there is an obstacle at (48, 50)
    // and should end up at (48, 49)

    test('Setup grid 100x100', () =>{
        api = new RobotAPI(new Vec2D(100, 100))
        expect(api.gridSize).toMatchObject(new Vec2D(100, 100))
    })

    test('Create robot at 50,50 facing North', () =>{
        api.createRobot(new Vec2D(50, 50), Direction.N)
        expect(api.robot.position).toMatchObject(new Vec2D(50, 50))
        expect(api.robot.direction).toBe(Direction.N)
    })

    test('Add obstacle at 48,50', () => {
        let coords:Vec2D[] = [new Vec2D(48, 50)]
        api.createObstaclesAt(coords)
        expect(api.obstacles[0].position).toMatchObject(coords[0])
    })

    test('Send instructions (fflffrbb) to robot and execute', () =>{
        let inst:string = 'fflffrbb'
        api.robot.pushInstructions(inst)
        // check instructions are correct
        expect(api.robot.instructions).toMatch(inst)
        // check that *NOT* all instructions could run
        expect(api.run()).toBe(false)
    })

    test('Check robot ends up at 48,49', () =>{  
        expect(api.robot.position).toMatchObject(new Vec2D(48, 49))
    })
})

describe('Bonus Scenario: allow grid to wrap', () => {
    // Ability to implement different rule sets for flat surface
    // or a globe where you warp when you get out of bounds

    test('Setup grid 3x3', () =>{
        api = new RobotAPI(new Vec2D(3, 3))
        expect(api.gridSize).toMatchObject(new Vec2D(3, 3))
    })

    test('Setup grid to wrap', () =>{
        api.gridWrap = true
        expect(api.gridWrap).toBe(true)
    })

    test('Create robot at 2,2 facing North', () =>{
        api.createRobot(new Vec2D(2, 2), Direction.N)
        expect(api.robot.position).toMatchObject(new Vec2D(2, 2))
        expect(api.robot.direction).toBe(Direction.N)
    })

    test('Send instructions (fff) to robot and check robot is at 2,3', () =>{
        let inst:string = 'fff'
        api.robot.pushInstructions(inst)
        expect(api.robot.instructions).toMatch(inst)
        expect(api.run()).toBe(true)
        expect(api.robot.position).toMatchObject(new Vec2D(2,3))
    })

    test('Send instructions (bbb) to robot and check robot is at 2,2', () =>{
        let inst:string = 'bbb'
        api.robot.pushInstructions(inst)
        expect(api.robot.instructions).toMatch(inst)
        expect(api.run()).toBe(true)
        expect(api.robot.position).toMatchObject(new Vec2D(2,2))
    })

    test('Send instructions (rff) to robot and check robot is at 0,2', () =>{
        let inst:string = 'rff'
        api.robot.pushInstructions(inst)
        expect(api.robot.instructions).toMatch(inst)
        expect(api.run()).toBe(true)
        expect(api.robot.position).toMatchObject(new Vec2D(0,2))
    })

    test('Send instructions (bb) to robot and check robot is at 2,2', () =>{
        let inst:string = 'bb'
        api.robot.pushInstructions(inst)
        expect(api.robot.instructions).toMatch(inst)
        expect(api.run()).toBe(true)
        expect(api.robot.position).toMatchObject(new Vec2D(2,2))
    })

})