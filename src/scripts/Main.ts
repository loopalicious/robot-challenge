import RobotAPI from "./RobotAPI"
import { Direction, Vec2D } from "./Utils"

export default class {
    constructor () {
        // instantiate api with grid size
        // note grid size is countable (0 counts as a space), so a 100x100 grid has 101x101 spaces
        const api = new RobotAPI(new Vec2D(100, 100))

        // toggle to allow flat/globe surface
        // true = globe surface
        // false = flat surface (default)
        api.gridWrap = true

        // add robot at (coords, facing direction)
        api.createRobot(new Vec2D( 50, 50), Direction.S)

        // add obstacles to grid
        api.createObstaclesAt([ new Vec2D(10, 10) ])

        // send instructions
        api.robot.pushInstructions('ffff')
        
        // execute instructions
        api.run()

        // log robot status
        console.log(api.robot.describe());
    }
}