import { Vec2D, Direction, InstructionMap } from './Utils'
import Entity from './Entity'

// Develop an api that moves a robot around on a grid (flat surface with defined size)
// Grid position (0, 0) should be the upper left corner
export default class {
    protected _gridSize:Vec2D
    protected _robot:Entity
    protected _obstacles:Entity[] = []
    public gridWrap:boolean = false

    constructor (gridSize:Vec2D) {
        //console.log('RobotAPI loaded');
        // validate that grid is positive in values and greater than 0
        if (gridSize.x < 2 || gridSize.y < 2) {
            throw new Error("invalid grid size, values need to be greater than 2x2")
        }
        this._gridSize = gridSize
    }

    get gridSize ():Vec2D {
        return this._gridSize
    }

    get robot ():Entity {
        return this._robot
    }

    get obstacles ():Entity[] {
        return this._obstacles
    }

    // You are given the initial starting point (x,y) of a robot 
    // and the direction (N,S,E,W) it is facing.
    createRobot (at:Vec2D, direction:Direction):void {
        // check that spawn point is within bounds
        if (at.x > this.gridSize.x || at.x < 0 ||
            at.y > this.gridSize.y || at.y < 0) {
                let msg = `Robot is being added out of bounds at: ${ JSON.stringify(at)}, and the grid is: ${JSON.stringify(this.gridSize)}`
                throw new Error(msg)
        }

        this._robot = new Entity(at, direction)
    }

    createObstaclesAt(coordsList:Vec2D[]):void {
        coordsList.forEach(coord => {
            this._obstacles.push( new Entity(coord, Direction.N))
        })
    }

    /**
     * Run all robot commands
     * @returns {boolean} true if all commands could be executed
     */
    run ():boolean {
        while (this.robot.instructions.length) {
            if (!this.instructionStep( this.robot.fetchNextInstruction() )) {
                console.log('last command did not succeed');
                return false
            }
        }
        return true
    }

    instructionStep (comm:string):boolean {
        console.log(this.robot.describe(), '- will execute:', comm, InstructionMap[comm]);

        let relTrans:Vec2D

        switch(comm) {
            // Implement commands that move the robot forward/backward (f,b).
            case 'f':
            case 'b':
                // setup relative transform to be added later to robot current position
                let x:number, y:number

                switch (this.robot.direction) {
                    case Direction.N:
                        x = 0, y = -1 * InstructionMap[comm]
                        break
                    case Direction.S:
                        x = 0, y = 1 * InstructionMap[comm]
                        break
                    case Direction.E:
                        x = 1 * InstructionMap[comm], y = 0
                        break
                    case Direction.W:
                        x = -1 * InstructionMap[comm], y = 0
                        break
                }
                relTrans = new Vec2D(x, y)
                break;
            // Implement commands that turn the robot left/right (l,r).
            case 'l':
            case 'r':
                // direction should always be 0..3
                // setup so modulus wraps even from negative values
                // this guarantees that if direction is -1 it will be offset to 3
                this.robot.direction = ((this.robot.direction + InstructionMap[comm]) % 4 + 4) % 4
                return true // nothing more to do, exit early
        }

        // check relative translation is setup, setup world coords
        if (typeof relTrans !== 'undefined') {
            // setup robot's next move in world coords         
            let nextPos = this.robot.position.add(relTrans)

            // "If a given sequence of commands encounters an obstacle or is out of bounds,
            // the robot should stop and report the obstacle. 
            // For example if it hits an obstacle or wall it should stop immediately 
            // without executing any more commands.

            // Bonus: Ability to implement different rule sets for flat surface 
            // or a globe where you warp when you get out of bounds
            // Implement detection if the new position is inside the bounds before moving to the new position.
            // check next move is within bounds
            if (nextPos.x > this.gridSize.x || nextPos.x < 0 ||
                nextPos.y > this.gridSize.y || nextPos.y < 0) {
                    // Out of bounds, check if flat/globe surface (no-wrap/wrap)
                    if (this.gridWrap) {
                        // wrap nextPos values to grid
                        let gridCountX = this.gridSize.x + 1
                        let gridCountY = this.gridSize.y + 1
                        nextPos.x = (nextPos.x % gridCountX + gridCountX) % gridCountX
                        nextPos.y = (nextPos.y % gridCountY + gridCountY) % gridCountY
                    } else {
                        let msg = `Robot cannot follow instruction "${comm}",
                               ${this.robot.describe()} -  
                               will move out of bounds at: ${JSON.stringify(nextPos)}`
                        console.log(msg);
                        return false
                    }
            }

            // Implement obstacle detection before each move to a new square.
            // Check through obstacles for any occupying the next position
            let collidedWith:Entity = this.obstacles.find(({position}) => position.equals(nextPos))
            
            // if obstacle found return false and exit early
            if (typeof collidedWith !== 'undefined') {
                let msg = `Robot cannot follow instruction "${comm}"
                           ${this.robot.describe()} -  
                           will collide with (obstacle) ${collidedWith.describe()}`
                console.log(msg);
                return false
            }

            this.robot.position = nextPos  
        }

        return true
    }
}