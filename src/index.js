import Main from './scripts/Main'

// Parcel HMR setup
if (module.hot) {
    module.hot.accept()
}

new Main()